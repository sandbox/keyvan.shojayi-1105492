<?php

function persianplus_settings_form(){
	$form = array();
	
	$form['persianplus_all_langs']=array(
		'#type' => 'checkbox',
		'#title' => t('enable in all language'),
		'#default_value' => variable_get('persianplus_all_langs', 0),
	);
	
	$form['persianplus_fix_arabic']=array(
		'#type' => 'checkbox',
		'#title' => t('convert arabic charachter to persian'),
		'#default_value' => variable_get('persianplus_fix_arabic', 1),
	);
	
	return system_settings_form($form);
}

